package io.daocloud.fileMerge.controller;

import io.daocloud.fileMerge.service.HdfsMergeFilesService;
import io.daocloud.fileMerge.service.LogMeaageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class FileMergeController {

    @Autowired
    private static HdfsMergeFilesService hdfsMergeFiles;

    @Autowired
    private LogMeaageService logMeaageService;

    /**
     * 手动触发任务
     */
    @RequestMapping("/startMerge")
    public static String getMessageInfo() {
//        hdfsMergeFiles.mergeByTenDays();
        return "合并成功.........";
    }

    /**
     * 日志内容
     * @return
     */
    @RequestMapping("/getMessageLogInfo")
    public List<String> getMessageLogInfo() {
        return logMeaageService.getMessageLogInfo();
    }
}

