package io.daocloud.fileMerge;

import io.daocloud.fileMerge.service.HdfsMergeFilesService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
public class HdfsfilesmergeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HdfsfilesmergeApplication.class, args);

		HdfsMergeFilesService.mergeLocalFiles();

	}

}
