package io.daocloud.fileMerge.service;


import io.daocloud.fileMerge.util.DateUtil;
import io.daocloud.fileMerge.util.HdfsUtil;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.EmptyFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by wangjingliang on 18/6/15.
 */
@Log4j
@Service
public class HdfsMergeFilesService{

    public static final String HDFSDir = "/data/2018-1-6/raw/";

    //SSD 路径
    private static final String url = "/data/evm/";

    //临时目录
    private static final String tmp = "/data/wangjingliang/tmp/";

    //Hdfs路径
    private static String hdfsPartUrl = "/user/wangjingliang/";
    //private static String hdfsPartUrl = "/data/2018-1-6/raw/";

    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private final static Calendar c = Calendar.getInstance();

    private static String beforeToday = "2018-10-31";

    private static String preMonday = "2018-11-10";

    public static List<String> dateByTen() {
        List<String> list = DateUtil.getEveryday(beforeToday, preMonday);
        return list;
    }


    /**
     * 合并10天的数据到hdfs
     * step
     * 1.读取ssd目录中的文件内容
     * 2.匹配hdfs对应的合并文件
     * 3.写入文件
     */
    public static void mergeLocalFiles() {
        Long a = System.currentTimeMillis();
        FileUtils.deleteQuietly(new File(tmp));
        log.info("delete tmp folder success.....");
        log.info("delete total time：" + (System.currentTimeMillis() - a) / 1000f + "s");
        log.info("start merge local files.....");
        List<String> list = dateByTen();
        for (int i = 0; i < list.size(); i++) {
                Iterator<File> files = FileUtils.iterateFiles(new File(url + list.get(i)), EmptyFileFilter.NOT_EMPTY, TrueFileFilter.INSTANCE);
            files.forEachRemaining(file -> {
                if (!file.getName().contains("_")) {
                    return;
                }
                String vin = file.getName().substring(0, file.getName().indexOf("_"));
                HdfsMergeFilesService.createDir(tmp + vin);
                String destFile = FilenameUtils.concat(tmp + vin, vin + "_" + beforeToday + "_" + preMonday);
                try {
                    FileUtils.writeLines(new File(destFile), FileUtils.readLines(file), true);
                } catch (IOException e) {
                    log.error("file write fail：", e);
                }
            });
        }
        log.info("dest files merge done .....");
        log.info("dest files merge total time：" + (System.currentTimeMillis() - a) / 1000f + "s");
    }


    private static boolean createDir(String destDirName) {
        File dir = new File(destDirName);
        if (dir.exists()) {
            return false;
        }
        //创建目录
        if (dir.mkdirs()) {
            log.info("-----create folder" + destDirName + "success.....");
            return true;
        } else {
            log.info("-----create folder" + destDirName + "fail.....");
            return false;
        }
    }

}